package com.ravi.test.util;

public enum ParentalControlLevel {

    MOVIE_U("U"),
    MOVIE_PG("PG"),
    MOVIE_TWELVE("12"),
    MOVIE_FIFTEEN("15"),
    MOVIE_EIGHTEEN("18");

    private String parentalControl;

    ParentalControlLevel(String parentalControl){
        this.parentalControl = parentalControl;
    }

    public String getParentalControl() {
        return parentalControl;
    }
}
