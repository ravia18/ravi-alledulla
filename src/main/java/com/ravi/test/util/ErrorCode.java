package com.ravi.test.util;

public enum ErrorCode {
    VALIDATION("error.validation", "Validation failed"),
    INTERNAL_ERROR("error.unexpected", "An unexpected error occurred"),
    INVALID_REQUEST("invalid.request", "An unexpected error occurred"),
    RESOURCE_NOT_FOUND("error.not_found", "Unable to locate the requested resource"),
    REMOTE_ERROR("error.service_unavailable", "Partner service is currently unavailable."),
    BAD_GATEWAY("error.bad_gateway", "Partner service returned an invalid response.");

    private String code;
    private String message;

    private ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}

