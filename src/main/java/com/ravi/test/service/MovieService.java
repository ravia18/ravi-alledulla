package com.ravi.test.service;

import com.ravi.test.exception.TechnicalFailureException;
import com.ravi.test.exception.TitleNotFoundException;

public interface MovieService {
    String getParentalControlLevel(String movieId) throws TitleNotFoundException, TechnicalFailureException;
}
