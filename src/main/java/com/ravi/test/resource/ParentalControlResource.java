package com.ravi.test.resource;

import com.ravi.test.exception.TechnicalFailureException;
import com.ravi.test.exception.TitleNotFoundException;
import com.ravi.test.service.MovieService;
import com.ravi.test.util.CheckDigit;
import com.ravi.test.util.ParentalControlLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParentalControlResource {

    private static final Logger LOG = LoggerFactory.getLogger(ParentalControlResource.class);


    private MovieService movieService;

    @Autowired
    public ParentalControlResource(MovieService movieService){
        this.movieService = movieService;
    }



    @GetMapping(path = "/parental-control/control/{parentalControl}/movie/{movieId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> findParentalControlByMovie(@PathVariable String parentalControl, @PathVariable String movieId) throws TechnicalFailureException, TitleNotFoundException {
        Boolean RESULT;

        if(parentalControl == null || movieId == null) {
            LOG.info("parentControlLevelPreference or movieId cannot be null");
            RESULT = false;
            return ResponseEntity.status(HttpStatus.OK).body(RESULT);
        }

        String accessResult =  movieService.getParentalControlLevel(movieId);

        if(CheckDigit.isNumber(accessResult) && CheckDigit.isNumber(parentalControl)){
            if(Integer.parseInt(parentalControl) < Integer.parseInt(accessResult)){
                RESULT = false;

            }else{
                RESULT = true;
            }
            return ResponseEntity.status(HttpStatus.OK).body(RESULT);
        }

        if (accessResult.equals(parentalControl) || accessResult.equals(ParentalControlLevel.MOVIE_U.getParentalControl())|| parentalControl.equals(ParentalControlLevel.MOVIE_U.getParentalControl()) || parentalControl.equals(ParentalControlLevel.MOVIE_PG.getParentalControl())) {
            RESULT = true;
        } else {
            RESULT = false;
        }

        return ResponseEntity.status(HttpStatus.OK).body(RESULT);
    }

    @GetMapping(path = "/parental-control/movie/{movieId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> findParentalControlByMovieAndNoAccessControl(@PathVariable String movieId) throws TechnicalFailureException, TitleNotFoundException {

        return findParentalControlByMovie(null,movieId);
    }

    @GetMapping(path = "/parental-control/control/{parentalControl}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> findParentalControlByAccessControlAndNoMovie(@PathVariable String parentalControl) throws TechnicalFailureException, TitleNotFoundException {

        return findParentalControlByMovie(parentalControl,null);
    }
}
