package com.ravi.test.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(
        code = HttpStatus.SERVICE_UNAVAILABLE
)
public class TechnicalFailureException extends Exception {

    public TechnicalFailureException(String message) {
        super(message);
    }
}
