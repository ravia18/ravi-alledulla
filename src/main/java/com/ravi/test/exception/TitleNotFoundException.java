package com.ravi.test.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(
        code = HttpStatus.NOT_FOUND
)
public class TitleNotFoundException extends Exception {

    private String code;

    public TitleNotFoundException() { }

    public TitleNotFoundException(String message) {
        super(message);
    }


    public TitleNotFoundException(String code, String message) {
        super(message);
        this.code = code;
    }


    public String getCode() {
        return code;
    }
}
