package com.ravi.test.resource;

import com.ravi.test.exception.TechnicalFailureException;
import com.ravi.test.exception.TitleNotFoundException;
import com.ravi.test.service.MovieService;
import com.ravi.test.util.ErrorCode;
import com.ravi.test.util.ParentalControlLevel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(ParentalControlResource.class)
@ComponentScan("com.ravi.test")
public class ParentalControlResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieService movieServiceMock;

    @Before
    public void setUp(){

    }


    @Test
    public void givenIPassParentalControlAndMovieThenIShouldGetTheAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel("movie1")).thenReturn("PG");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","PG","movie1").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                                       .andExpect(content().string(containsString("true")));

    }

    @Test
    public void givenIPassParentalControlAndInvalidMovieThenIShouldGetResourceNotFound() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel("movieXYZ")).thenThrow(new TitleNotFoundException(ErrorCode.RESOURCE_NOT_FOUND.getCode(), "Invalid Movie id movieXYZ"));
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","PG","movieXYZ").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isNotFound());
    }


    @Test
    public void givenIPassParentalControlAndValidMovieWhenMovieServiceIsDownThenIShouldGetSystemError() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel("movieXYZ")).thenThrow(new TechnicalFailureException("System Error"));
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","PG","movieXYZ").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().is5xxServerError());
    }


    @Test
    public void givenIPassNullParentalControlAndValidMovieThenIShouldGetNoAccessResult() throws Exception {

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/movie/{movieId}","movieXYZ").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                                       .andExpect(content().string(containsString("false")));

    }

    @Test
    public void givenIPassValidParentalControlAndNullMovieThenIShouldGetNoAccessResult() throws Exception {

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}","PG").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("false")));

    }

    @Test
    public void givenIPassUParentalControlAnd18MovieThenIShouldGetTheAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_EIGHTEEN.name())).thenReturn(ParentalControlLevel.MOVIE_EIGHTEEN.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","U","MOVIE_EIGHTEEN").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

    }

    @Test
    public void givenIPassUParentalControlAnd12MovieThenIShouldGetTheAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_TWELVE.name())).thenReturn(ParentalControlLevel.MOVIE_TWELVE.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","U","MOVIE_TWELVE").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

    }

    @Test
    public void givenIPassUParentalControlAnd15MovieThenIShouldGetTheAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_FIFTEEN.name())).thenReturn(ParentalControlLevel.MOVIE_FIFTEEN.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","U","MOVIE_FIFTEEN").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

    }

    @Test
    public void givenIPassUParentalControlAndPGMovieThenIShouldGetTheAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_PG.name())).thenReturn(ParentalControlLevel.MOVIE_PG.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","U","MOVIE_PG").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

    }

    @Test
    public void givenIPassPGParentalControlAndUMovieThenIShouldGetTheAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_U.name())).thenReturn(ParentalControlLevel.MOVIE_U.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","PG","MOVIE_U").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

    }

    @Test
    public void givenIPassPGParentalControlAnd15MovieThenIShouldGetTheAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_FIFTEEN.name())).thenReturn(ParentalControlLevel.MOVIE_FIFTEEN.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","PG","MOVIE_FIFTEEN").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

    }

    @Test
    public void givenIPass15ParentalControlAnd18MovieThenIShouldGetNoAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_EIGHTEEN.name())).thenReturn(ParentalControlLevel.MOVIE_EIGHTEEN.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","15","MOVIE_EIGHTEEN").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("false")));

    }

    @Test
    public void givenIPass18ParentalControlAnd15MovieThenIShouldGetAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_FIFTEEN.name())).thenReturn(ParentalControlLevel.MOVIE_FIFTEEN.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","18","MOVIE_FIFTEEN").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

    }

    @Test
    public void givenIPass12ParentalControlAnd15MovieThenIShouldGetNoAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_FIFTEEN.name())).thenReturn(ParentalControlLevel.MOVIE_FIFTEEN.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","12","MOVIE_FIFTEEN").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("false")));

    }

    @Test
    public void givenIPass15ParentalControlAnd12MovieThenIShouldGetAccessResult() throws Exception {
        Mockito.when(movieServiceMock.getParentalControlLevel(ParentalControlLevel.MOVIE_TWELVE.name())).thenReturn(ParentalControlLevel.MOVIE_TWELVE.getParentalControl());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/parental-control/control/{parentalControl}/movie/{movieId}","15","MOVIE_TWELVE").accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(content().string(containsString("true")));

    }



}
