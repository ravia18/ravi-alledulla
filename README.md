# parental-control service

1.Provided the API implementation for ParentalControlResource
2.Not provided any implementation for MovieService.Used the Mock for MovieService implementation
3.Used spring mvcMock to test the API end points.
4.Please run the unit test ParentalControlResourceTest to run the unit test.
5.I have covered most of the scenarios using unit tests.
6.Requires Maven 3.3, java8, spring boot, spring test dependencies.
7.run mvn clean install to build and run the unit tests from command prompt.
8.Provided implementation for some utitlity classes as felt required for tests.
9. I have used the root project as my name as suggested in the doc.
10. plesae clone the project from following location 
https://ravia18@bitbucket.org/ravia18/ravi-alledulla.git
